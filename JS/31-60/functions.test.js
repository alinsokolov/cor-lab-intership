const{
    swap,
    minmax,
    sortInc3,
    sortDec3,
    shiftRight3,
    shiftLeft3,
    power1,
    power2,
    power3,
    exp1,
    sin1,
    cos1,
    ln1,
    atan1,
    power4,
    gcd2,
    frac1,
    lcm2,
    gcd3,
    timeToHMS,
    incTime,
    isLeapYear,
    monthDays,
    prevDate,
    nextDate, 
    leng,
    perim,
    area,
    dist,
    alts
} = require("./functions")

//f31
describe("f31", ()=>{
    describe("swap", ()=>{
        it("j&i sunt valide", ()=>{
            const result = swap([1,2,3,4],1,2);

            expect(result).toEqual([2,1,3,4]);
        })

        it("j&i sunt invalide (fractii)", ()=>{
            const result = swap([1,2,3,4],1.2,2);

            expect(result).toEqual("invalid numbers");
        })

        it("j&i sunt invalide (negative)", ()=>{
            const result = swap([1,2,3,4],-1,2);

            expect(result).toEqual("invalid numbers");
        })
    })
})


//f32
describe("f32", ()=>{
    describe("minMax", ()=>{
        it("j&i sunt valide", ()=>{
            const result = minmax([1,2,3,4],1,2);

            expect(result).toEqual([2,1,3,4]);
        })

        it("j&i sunt invalide (fractii)", ()=>{
            const result = minmax([1,2,3,4],1.2,2);

            expect(result).toEqual("invalid numbers");
        })

        it("j&i sunt invalide (negative)", ()=>{
            const result = minmax([1,2,3,4],-1,2);

            expect(result).toEqual("invalid numbers");
        })
    })
})


//f33
describe("f33", ()=>{
    describe("sortInc3", ()=>{
        it("Numere valide", ()=>{
            const result = sortInc3([13,-1,7]);

            expect(result).toEqual([-1,7,13]);
        })
    })
})


//34
describe("f34", ()=>{
    describe("sortDec3", ()=>{
        it("Numere valide", ()=>{
            const result = sortDec3([13,-1,7]);

            expect(result).toEqual([13,7,-1]);
        })
    })
})


//35
describe("f35", ()=>{
    describe("shiftRight3", ()=>{
        it("Numere valide", ()=>{
            const result = shiftRight3([1,2,3]);

            expect(result).toEqual([3,1,2]);
        })
    })
})


//36
describe("f36", ()=>{
    describe("shiftLeft3", ()=>{
        it("Numere valide", ()=>{
            const result = shiftLeft3([1,2,3]);

            expect(result).toEqual([2,3,1]);
        })
    })
})


//37
describe("f37", ()=>{
    describe("power1", ()=>{
        it("n^3", ()=>{
            const result = power1(3,"n");

            expect(result).toEqual("invalid numbers");
        })

        it("3^3", ()=>{
            const result = power1(3,3);

            expect(result).toEqual(27);
        })

        it("10 ^ -1", ()=>{
            const result = power1(10,-1);

            expect(result).toEqual(0.1);
        })

        it("10 ^ 0", ()=>{
            const result = power1(10,0);

            expect(result).toEqual(1);
        })
    })
})



//38
describe("f38", ()=>{
    describe("power2", ()=>{
        it("2^3", ()=>{
            const result = power2(2,0);

            expect(result).toEqual(1);
        })

        it("3^3", ()=>{
            const result = power2(3,3);

            expect(result).toEqual(27);
        })

        it("10 ^ -1", ()=>{
            const result = power2(10,-1);

            expect(result).toEqual(0.1);
        })
    })
})



//39
describe("f39", ()=>{
    describe("power3", ()=>{
        it("5^2", ()=>{
            const result = power3(5,2);

            expect(result).toEqual(25);
        })

        it("1^1.03", ()=>{
            const result = power3(1,1.03);

            expect(result).toEqual(1);
        })

        it("10 ^ -1", ()=>{
            const result = power3(10,-1);

            expect(result).toEqual(0.1);
        })
    })
})



//40
describe("f40", () => {
    describe("exp1", () => {
        it("x=1, eps = 0.5", () => {
            const result = exp1(1, 0.5);

            expect(result).toEqual(2.5);
        });

        it("x= 2.5, eps = 0.9", () => {
            const result = exp1(2.5, 0.5);

            expect(result).toEqual(12.01);
        });

        it("eps negative", () => {
            const result = exp1(4, -3);

            expect(result).toEqual("invalid eps");
        });
    });
});



//41
describe("f41", () => {
    describe("sin1", () => {
        it("x=3, eps = 1", () => {
            const result = sin1(3, 1);

            expect(result).toEqual(-4.5);
        });

        it("eps = 0", () => {
            const result = sin1(2.5, 0);

            expect(result).toEqual("invalid eps");
        });

        it("eps negative", () => {
            const result = sin1(4, -3);

            expect(result).toEqual("invalid eps");
        });
    });
});



//42
describe("f42", () => {
    describe("cos1", () => {
        it("x=1, eps = 3", () => {
            const result = cos1(1, 3);

            expect(result).toEqual(-0.5);
        });

        it("eps = 0", () => {
            const result = cos1(2.5, 0);

            expect(result).toEqual("invalid eps");
        });

        it("eps negative", () => {
            const result = cos1(4, -3);

            expect(result).toEqual("invalid eps");
        });
    });
});



//43
describe("f43", () => {
    describe("ln1", () => {
        it("x=0.5, eps = 3", () => {
            const result = ln1(0.5, 3);

            expect(result).toEqual(-0.125);
        });

        it("eps = 0", () => {
            const result = ln1(2.5, 0);

            expect(result).toEqual("invalid numbers");
        });

        it("eps negative", () => {
            const result = ln1(4, -3);

            expect(result).toEqual("invalid numbers");
        });

        it("x = 1", () => {
            const result = ln1(1, 3);

            expect(result).toEqual("invalid numbers");
        });

        it("x > 1", () => {
            const result = ln1(4, 5);

            expect(result).toEqual("invalid numbers");
        });
    });
});



//44
describe("f44", () => {
    describe("atan1", () => {
        it("x=0.5, eps = 3", () => {
            const result = atan1(0.5, 3);

            expect(result).toEqual(-0.042);
        });

        it("eps = 0", () => {
            const result = atan1(2.5, 0);

            expect(result).toEqual("invalid numbers");
        });

        it("eps negative", () => {
            const result = atan1(4, -3);

            expect(result).toEqual("invalid numbers");
        });

        it("x = 1", () => {
            const result = atan1(1, 3);

            expect(result).toEqual("invalid numbers");
        });

        it("x > 1", () => {
            const result = atan1(4, 5);

            expect(result).toEqual("invalid numbers");
        });
    });
});



//45
describe("f45", () => {
    describe("power4", () => {
        it("x=0.5, a = 10, eps = 3", () => {
            const result = power4(0.5, 10, 3);

            expect(result).toEqual(1.125);
        });

        it("eps = 0", () => {
            const result = power4(2.5, 0);

            expect(result).toEqual("invalid numbers");
        });

        it("eps negative", () => {
            const result = power4(4, -3);

            expect(result).toEqual("invalid numbers");
        });

        it("x = 1", () => {
            const result = power4(1, 3);

            expect(result).toEqual("invalid numbers");
        });

        it("x > 1", () => {
            const result = atan1(4, 5);

            expect(result).toEqual("invalid numbers");
        });
    });
});



//46
describe("f46", ()=>{
    describe("gcd2", ()=>{
        it("a=2, b=4", ()=>{
            const result = gcd2(2,4);

            expect(result).toEqual(2);
        })

        it("a=5, b=3", ()=>{
            const result = gcd2(5,3);

            expect(result).toEqual(1);
        })

        it("a=6, b=9", ()=>{
            const result = gcd2(6,9);

            expect(result).toEqual(3);
        })
    })
})



//47
describe("f47", ()=>{
    describe("frac1", ()=>{
        it("a=2, b=4", ()=>{
            const result = frac1(2,4);

            expect(result).toEqual([1,2]);
        })

        it("a=5, b=3", ()=>{
            const result = frac1(5,3);

            expect(result).toEqual([5,3]);
        })

        it("b = 0", ()=>{
            const result = frac1(6,0);

            expect(result).toEqual("error: numarator null");
        })
    })
})



//48
describe("f48", ()=>{
    describe("lcm2", ()=>{
        it("a=2, b=4", ()=>{
            const result = lcm2(2,4);

            expect(result).toEqual(4);
        })

        it("a=5, b=3", ()=>{
            const result = lcm2(5,3);

            expect(result).toEqual(15);
        })

        it("not integer numbers", ()=>{
            const result = lcm2(6,0.5);

            expect(result).toEqual("error: invalid numbers");
        })
    })
})



//49
describe("f49", ()=>{
    describe("gcd3", ()=>{
        it("a=10, b=4, c=6", ()=>{
            const result = gcd3(10,4,6);

            expect(result).toEqual(2);
        })

        it("a=5, b=15, c=30", ()=>{
            const result = gcd3(5,15,30);

            expect(result).toEqual(5);
        })

        it("a=6, b=9, c=12", ()=>{
            const result = gcd3(6,9,12);

            expect(result).toEqual(3);
        })
    })
})



//50
describe("f50", ()=>{
    describe("gcd3", ()=>{
        it("negative time", ()=>{
            const result = timeToHMS(-20);

            expect(result).toEqual("error: negative time");
        })

        it("3600 seconds", ()=>{
            const result = timeToHMS(3600);

            expect(result).toEqual({ hours: 1, minutes: 0, seconds: 0 });
        })

        it("3800 seconds", ()=>{
            const result = timeToHMS(3800);

            expect(result).toEqual({ hours: 1, minutes: 3, seconds: 20 });
        })
    })
})



//51
describe("f51", ()=>{
    describe("incTime", ()=>{
        it("negative time", ()=>{
            const result = incTime(-20,1,2,3);

            expect(result).toEqual("error: negative time");
        })

        it("1h, 1m, 1s) + 59 min ", ()=>{
            const result = incTime(1,1,1,3540);

            expect(result).toEqual({ hours: 2, minutes: 0, seconds: 1 });
        })

        it("(0h, 59m, 3s) + 57sec", ()=>{
            const result = incTime(0,59,3,57);

            expect(result).toEqual({ hours: 1, minutes: 0, seconds: 0 });
        })
    })
})



//52
describe("f52", ()=>{
    describe("isLeapYear", ()=>{
        it("invalid year", ()=>{
            const result = isLeapYear(-2000);

            expect(result).toEqual("error: invalid year");
        })

        it("2000", ()=>{
            const result = isLeapYear(2000);

            expect(result).toEqual(true);
        })

        it("2001", ()=>{
            const result = isLeapYear(2001);

            expect(result).toEqual(false);
        })

        it("1848", ()=>{
            const result = isLeapYear(1848);

            expect(result).toEqual(true);
        })
    })
})



//53
describe("f53", ()=>{
    describe("monthDays", ()=>{
        it("negative valors", ()=>{
            const result = monthDays(-2,2000);

            expect(result).toEqual("error: invalid valors");
        })

        it("29", ()=>{
            const result = monthDays(2,2012);

            expect(result).toEqual(29);
        })

        it("2001", ()=>{
            const result = monthDays(10,2000);

            expect(result).toEqual(31);
        })
    })
})



//54
describe("f54", ()=>{
    describe("prevDate", ()=>{

        it("negative valors", ()=>{
            const result = prevDate(-2000,3,5);

            expect(result).toEqual("error: invalid valors");
        })

        it("03.06.2021", ()=>{
            const result = prevDate(2021,6,3);

            expect(result).toEqual("2021-06-02");
        })

        it("01.03.2020 (an bisect)", ()=>{
            const result = prevDate(2020,3,1);

            expect(result).toEqual("2020-02-29");
        })
    })
})




//55
describe("f55", ()=>{
    describe("nextDate", ()=>{

        it("negative valors", ()=>{
            const result = nextDate(-2000,3,5);

            expect(result).toEqual("error: invalid valors");
        })

        it("03.06.2021", ()=>{
            const result = nextDate(2021,6,3);

            expect(result).toEqual("2021-06-04");
        })

        it("01.03.2020 (an bisect)", ()=>{
            const result = nextDate(2020,2,28);

            expect(result).toEqual("2020-02-29");
        })
    })
})



//56
describe("f56", ()=>{
    describe("leng", ()=>{

        it("xA = -3, yA = -2, xB = 3, yB =3", ()=>{
            const result = leng(-3,-2,3,3);

            expect(result).toEqual(7.81);
        })

        it("xA = 4, yA = 0, xB = -3, yB = 6", ()=>{
            const result = leng(4,0,-3,6);

            expect(result).toEqual(9.22);
        })
    })
})



//57
describe("f57", ()=>{
    describe("perim", ()=>{

        it("xA = -3, yA = -2, xB = 3, yB =3, xC = 3, yC = -3", ()=>{
            const result = perim(-3,-2,3,3,3,-3);

            expect(result).toEqual(19.89);
        })

        it("xA = 4, yA = 0, xB = -3, yB = 6, xC = 3, yC = -3", ()=>{
            const result = perim(4,0,-3,6,3,3);

            expect(result).toEqual(19.09);
        })
    })
})



//58
describe("f58", ()=>{
    describe("area", ()=>{

        it("xA = -3, yA = -2, xB = 3, yB =3, xC = 3, yC = -3", ()=>{
            const result = area(-3,-2,3,3,3,-3);

            expect(result).toEqual(17.99);
        })

        it("xA = 4, yA = 0, xB = -3, yB = 6, xC = 3, yC = -3", ()=>{
            const result = area(4,0,-3,6,3,3);

            expect(result).toEqual(7.49);
        })
    })
})



//59
describe("f59", ()=>{
    describe("dist", ()=>{

        it("xP = 3, yP = -3, xA = -3, yA = -2, xB = 3, yB =3", ()=>{
            const result = dist(3,-3,-3,-2,3,3);

            expect(result).toEqual(4.61);
        })

        it(" xC = 3, yC = -3, xA = 4, yA = 0, xB = -3, yB = 6", ()=>{
            const result = dist(3,-3,4,0,-3,6);

            expect(result).toEqual(2.92);
        })
    })
})



//60
describe("f60", ()=>{
    describe("alts", ()=>{

        it("xA = -3, yA = -2, xB = 3, yB =3, xC = 3, yC = -3", ()=>{
            const result = alts(-3,-2,3,3,3,-3);

            expect(result).toEqual({"h_A": 6, "h_B": 5.92, "h_C": 4.61});
        })

        it("xA = 4, yA = 0, xB = -3, yB = 6, xC = 3, yC = -3", ()=>{
            const result = alts(4,0,-3,6,3,3);

            expect(result).toEqual({"h_A": 2.23, "h_B": 4.74, "h_C": 1.62});
        })
    })
})



