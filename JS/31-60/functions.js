//f31
/** Write a function Swap(X, I, J) that exchanges the values of items X_I and X_J of a list X of real numbers (I and J are input integer 
 * parameters, the function returns the None value). Having input a list of four real numbers and using three calls of this function, 
 * sequentially exchange the values of the two first, two last, and two middle items of the given list. Output the new values of the list.
 */
function swap(x,i,j){
    if(i<0 || i>x.length || j<0 || j>x.length){
         return "invalid numbers"
       }
   
     if((i+j) % 2 == 0 || (i+j) % 2 == 1){
       const aux=x[i-1];
       x[i-1] = x[j-1];
       x[j-1] = aux;
       return x
     }

     return "invalid numbers"
   }
  

//f32
/** Write a function Minmax(X, I, J) that writes the minimal value of items X_I and X_J of a list X to the item X_I and writes the maximal 
 * value of these items to the item X_J (I and J are input integer parameters, the function returns the None value). Using four calls of 
 * this function, find the minimal value and the maximal value among four given real numbers. */

function minmax(x,i,j){
    if(i<0 || i>x.length || j<0 || j>x.length){
        return "invalid numbers"
    }
    if((i+j) % 2 == 0 || (i+j) % 2 == 1){
        max = Math.max(x[i-1],x[j-1])
        min = Math.min(x[i-1],x[j-1])
        x[i-1] = max;
        x[j-1] = min;
        
        return x
    }

    return "invalid numbers"
}

//f33
/** Write a function SortInc3(X) that sorts the list X of three real-valued items in ascending order (the function returns the None value). 
 * Using this function, sort each of two given lists X and Y.
 */
function sortInc3(x){
    return x.sort((a, b) => a - b)
  }


//f34
/** Write a function SortDec3(X) that sorts the list X of three real-valued items in descending order (the function returns the None value). 
 * Using this function, sort each of two given lists X and Y.
 */
function sortDec3(x){
    return x.sort((a, b) => b - a)
  }


//f35
/** Write a function ShiftRight3(X) that performs a right cyclic shift of a list X of three real-valued items: the value of each item 
 * should be assigned to the next item and the value of the last item should be assigned to the first item (the function returns the None
 * value). Using this function, perform the right cyclic shift for each of two given lists X and Y. */
function shiftRight3(x){
    const ultim = x.pop();
    x.unshift(ultim);
    return x;
}


//36
/** Write a real-valued function Power1(A, B) that returns the power A^B calculated by the formula A^B = exp(B·ln(A)) (A and B are 
 * real-valued parameters). In the case of zero-valued or negative parameter A the function returns 0. Having input real numbers 
 * P, A, B, C and using this function, find the powers A^P, B^P, C^P. */
function shiftLeft3(x){
    let primEl = x.shift();
    x.push(primEl);
    return x;
}


//37
/** Write a real-valued function Power1(A, B) that returns the power A^B calculated by the formula A^B = exp(B·ln(A)) (A and B are 
 * real-valued parameters). In the case of zero-valued or negative parameter A the function returns 0. Having input real numbers 
 * P, A, B, C and using this function, find the powers A^P, B^P, C^P. */
function power1(a,b){
    if(isNaN(a) || isNaN(b)){
        return "invalid numbers"
    }
    return a**b;
}


//38
/** Write a real-valued function Power2(A, N) that returns the power A^N calculated by the following formulas:

A^0 = 1;
A^N = A·A·…·A    (N factors),    if N > 0;
A^N = 1/(A·A·…·A)    (|N| factors),    if N < 0

(A is a real-valued parameter, N is an integer parameter). Having input a real number A and integers K, L, M and using this function, 
find the powers A^K, A^L, A^M. */

function power2(a, n) {
    if(n>=0){
        return a**n;
    }
    return 1/(a** Math.abs(n));    
}



//39
/** Using the Power1 and Power2 functions (see Func37 and Func38), write a real-valued function Power3(A, B) that returns the power A^B 
 * calculated as follows (A and B are real-valued parameters): if B has a zero-valued fractional part then the function Power2(A, N) is 
 * called (an integer variable N is equal to B), otherwise the function Power1(A, B) is called. Having input real numbers P, A, B, C and 
 * using the Power3 function, find the powers A^P, B^P, C^P. */

function power3(a,b){
    if(Number.isInteger(b)){
      return power2(a,b)
    }
    else{
      return power1(a,b)
    }
}


//factorial
function factorial(num){
    if (num == 0) 
        return 1;
    return (num * factorial(num - 1));
}


//40
/** Write a real-valued function Exp1(x, ε) (x and ε are real numbers, ε > 0) that returns an approximate value of the function exp(x) 
defined as follows: exp(x) = 1 + x + x^2/(2!) + x^3/(3!) + … + x^n/(n!) + …
(n! = 1·2·…·n). Stop adding new terms to the sum when the value of the next term will be less than ε. Using this function, find the 
approximate values of the function exp(x) at a given point x for six given ε. */

function  exp1(x,eps){
    if (eps<=0){
        return "invalid eps"
    }

    let sum=1
    let expX=1  

    for(let n=1; expX>eps; n++) {
        expX = x**n / factorial(n)
        sum += expX
    }

    return Number(sum.toFixed(3));
}



//41
/** Scrieți o funcție cu valoare reală Sin1 (x, ε) (x și ε sunt numere reale, ε> 0) care returnează o valoare aproximativă a funcției 
 * sin (x) definită după cum urmează:

sin (x) = x - x ^ 3 / (3!) + x ^ 5 / (5!) -… + (−1) ^ n · x ^ (2 · n + 1) / ((2 · n + 1)!) +….

Nu mai adăugați termeni noi la sumă atunci când valoarea absolută a termenului următor va fi mai mică de ε. Folosind această funcție,
găsiți valorile aproximative ale funcției sin (x) la un punct dat x pentru șase date ε. */

function sin1(x,eps){
    if (eps<=0){
        return "invalid eps"
    }
    nextTermn = eps;
    let n=1;
    let sinX=0;
    while(nextTermn>=eps){
        nextTermn = (-1)**n * x**(2*n + 1) / factorial(2*n +1)
        sinX =+ nextTermn
        n++
    }
    return sinX
}



//42
/** Scrieți o funcție cu valoare reală Cos1 (x, ε) (x și ε sunt numere reale, ε> 0) care returnează o valoare aproximativă a funcției cos (x) definită după cum urmează:
cos (x) = 1 - x ^ 2 / (2!) + x ^ 4 / (4!) -… + (−1) ^ n · x ^ (2 · n) / ((2 · n)!) +….
Nu mai adăugați termeni noi la sumă atunci când valoarea absolută a termenului următor va fi mai mică de ε. Folosind această funcție,
găsiți valorile aproximative ale funcției cos (x) la un punct dat x pentru șase date ε. **/

function cos1(x,eps){
    if (eps<=0){
        return "invalid eps"
    }
    nextTermn = eps;
   let n=1;
   let cosX=0;
    while(nextTermn>=eps){
        nextTermn = (-1)**n * x**(2*n) / factorial(2*n)
        cosX =+ nextTermn
        n++
    }
    return cosX
}



//43
/** Scrieți o funcție cu valoare reală Ln1 (x, ε) (x și ε sunt numere reale, | x | <1, ε> 0) care returnează o valoare aproximativă a 
funcției ln (1 + x) definită după cum urmează: ln (1 + x) = x - x ^ 2/2 + x ^ 3/3 -… + (−1) ^ n · x ^ (n + 1) / (n + 1) +….
Nu mai adăugați termeni noi la sumă atunci când valoarea absolută a termenului următor va fi mai mică de ε. Folosind această funcție,
găsiți valorile aproximative ale funcției ln (1 + x) la un punct dat x pentru șase date ε. **/

function ln1(x,eps){
    if (eps<=0 || Math.abs(x)>=1){
        return "invalid numbers"
    }
    nextTermn = eps;
    let n=1;
    let lnX=0;
    while(nextTermn>=eps){
        nextTermn = (-1)**n * x**(n+1) / (n+1)
        lnX =+ nextTermn
        n++
    }
    return lnX
}


//44
/** Scrieți o funcție cu valoare reală Atan1 (x, ε) (x și ε sunt numere reale, | x | <1, ε> 0) care returnează o valoare aproximativă a 
funcției atan (x) definită după cum urmează: atan (x) = x - x ^ 3/3 + x ^ 5/5 -… + (−1) ^ n · x ^ (2 · n + 1) / (2 · n + 1) +….
Nu mai adăugați termeni noi la sumă atunci când valoarea absolută a termenului următor va fi mai mică de ε. Folosind această funcție,
găsiți valorile aproximative ale funcției atan (x) la un punct dat x pentru șase date ε. */

function atan1(x,eps){
    if (eps<=0 || Math.abs(x)>=1){
        return "invalid numbers"
    }
    nextTermn = eps;
    let n=1;
    let atanX=0;
    while(nextTermn>=eps){
        nextTermn = (-1)**n * x**(2*n +1) / (2*n +1)
        atanX =+ nextTermn
        n++
    }
    return Number(atanX.toFixed(3))
}



//45
/** Scrieți o funcție cu valoare reală Power4 (x, a, ε) (x, a, ε sunt numere reale, | x | <1, a, ε> 0) care returnează o valoare 
aproximativă a funcției (1 + x) a definit ca: 
(1 + x) ^ a = 1 + a · x + a · (a − 1) · x ^ 2 / (2!) + ... + a · (a − 1) · ... · (a − n + 1) · X ^ n / (n!) +….
Nu mai adăugați termeni noi la sumă atunci când valoarea absolută a termenului următor va fi mai mică de ε.
Folosind această funcție, găsiți valorile aproximative ale funcției (1 + x) a la un punct dat x pentru un exponent dat a și șase date ε. */

function power4(x,a,eps){
    if (eps<=0 || Math.abs(x)>=1){
        return "invalid numbers"
    }
    nextTermn = eps;
    let n=1;
    let pow=0;
    while(nextTermn>=eps){
        nextTermn = (a - n + 1) * x**n / factorial(n)
        pow =+ nextTermn
        n++
    }
    return Number(pow.toFixed(3))
}



//46
/** Write an integer function GCD2(A, B) that returns the greatest common divisor (GCD) of two positive integers A and B. Use the Euclidean 
algorithm: GCD(A, B) = GCD(B, A mod B),    if B ≠ 0;        GCD(A, 0) = A,
where "mod" denotes the operator of taking the remainder after integer division. Using this function, find the greatest common divisor for 
each of pairs (A, B), (A, C), (A, D) provided that integers A, B, C, D are given. */

function gcd2(a,b){
    if (b == 0) {
        return a;
    }
    return gcd2(b, a % b);
}



//47
/** Using the GCD2 function from the task Func46, write a procedure Frac1(a, b, p, q), that simplifies a fraction a/b to the irreducible 
form p/q (a and b are input integer parameters, p and q are output integer parameters). The sign of a resulting fraction p/q is assigned 
to its numerator, so q > 0. Using this procedure, find the numerator and the denominator for each of irreducible fractions a/b + c/d, 
 a/b + e/f, a/b + g/h provided that integers a, b, c, d, e, f, g, h are given. */

function frac1(a,b){
    if(b == 0){
        return "error: numarator null"
    }
    const simpl = gcd2(a,b);
    const p = a/simpl;
    const q = b/simpl;
    return [p,q];
}



//48
/** Taking into account that the least common multiple of two positive integers A and B equals A·(B/GCD(A, B)), where GCD(A, B) is the 
greatest common divisor of A and B, and using the GCD2 function from the task Func46, write an integer function LCM2(A, B) that returns 
the least common multiple of A and B. Using this function, find the least common multiple for each of pairs (A, B), (A, C), (A, D) 
provided that integers A, B, C, D are given. */

function lcm2 (a, b){
    if(Number.isInteger(a) && Number.isInteger(b)){
      return a*(b/gcd2(a,b));
    }
    else return "error: invalid numbers"
  }


//49
/** Taking into account the formula GCD(A, B, C) = GCD(GCD(A, B), C) and using the GCD2 function from the task Func46, write an integer 
function GCD3(A, B, C) that returns the greatest common divisor of three positive integers A, B, C. Using this function, find the 
greatest common divisor for each of triples (A, B, C), (A, C, D), (B, C, D) provided that integers A, B, C, D are given. */

function gcd3(a,b,c){
    return (gcd2(gcd2(a,b),c))
}


//50
/** Write a function TimeToHMS(T) that converts a time interval T (in seconds) into the "hours H, minutes M, seconds S" format and returns 
 the values H, M, S (T, H, M, S are integers). Using this function, find the amount of hours, minutes and seconds for each of five given 
 time intervals T_1, T_2, …, T_5.
 */

function timeToHMS(t){
    if(t<0 ){
        return "error: negative time"
    }

    const h = t/3600
    const m = (h - Math.trunc(h))*60
    const s = (m - Math.trunc(m))*60

    return {
      hours: Math.trunc(h),
      minutes: Math.trunc(m),
      seconds: Math.trunc(s)
    }
}


//51
/** Write a function IncTime(H, M, S, T) that increases a time interval in hours H, minutes M, seconds S on T seconds and returns new 
values of hours, minutes, and seconds (all numbers are positive integers). Having input hours H, minutes M, seconds S (as integers) 
and an integer T and using the IncTime function, increase the given time interval on T seconds and output new values of H, M, S. */

function incTime(h,m,s,t){
    if(h<0 || m<0 || s<0 || t<0){
        return "error: negative time"
    }
    const time = h*3600 + m*60 + s + t
    return timeToHMS(time)
}



//52
/** Write a logical function IsLeapYear(Y) that returns True if a year Y (a positive integer parameter) is a leap year, and False otherwise. 
Output the return values of this function for five given values of the parameter Y. Note that a year is a leap year if it is divisible by 
4 except for years that are divisible by 100 and are not divisible by 400 */

function isLeapYear(y){
    if(!Number.isInteger(y) || y<0){
      return "error: invalid year"
    }
    if(y%4 == 0){
      if(y%100 == 0 && y%400 != 0){
        return false;
      }
      else return true;
      }
      return false;
  }



//53
/** Using the IsLeapYear function from the task Func52, write an integer function MonthDays(M, Y) that returns the amount of days for M-th 
month of year Y (M and Y are integers, 1 ≤ M ≤ 12, Y > 0). Output the return value of this function for a given year Y and each of given 
months M_1, M_2, M_3.
 */

function monthDays(m,y){
    if(!Number.isInteger(y) || y<0 || m<0 || m>12 || !Number.isInteger(m)){
        return "error: invalid valors"
    }
    return new Date(y,m,0).getDate();
}


//54
/** Folosind funcția MonthDays din sarcina Func53, scrieți o funcție PrevDate (D, M, Y) care schimbă o dată corectă, reprezentată la 
formatul „ziua D, numărul lunii M, anul Y”, la una anterioară și returnează valori noi de zi, lună și an (toate numerele sunt întregi). 
Aplicați această funcție la trei date date și ieșiți pe cele anterioare rezultate. */

function prevDate(y,m,d){
    if(!Number.isInteger(y) || y<0 || m<0 || m>12 || !Number.isInteger(m) || d<1 || d>31){
        return "error: invalid valors"
    }

    let date=new Date(y,m,d)
    date.setUTCFullYear(y)
    date.setUTCMonth(m-1)
    date.setUTCDate(d-1);

    return date.toISOString().substr(0,10);
}



//55
/** Using the MonthDays function from the task Func53, write a function NextDate(D, M, Y) that changes a correct date, represented at 
the "day D, month number M, year Y" format, to a next one and returns new values of day, month, and year (all numbers are integers). 
Apply this function to three given dates and output resulting next ones. */

function nextDate(y,m,d){
    if(!Number.isInteger(y) || y<0 || m<0 || m>12 || !Number.isInteger(m) || d<1 || d>31){
        return "error: invalid valors"
    }

    let date=new Date(y,m,d)
    date.setUTCFullYear(y)
    date.setUTCMonth(m-1)
    date.setUTCDate(d+1);

    return date.toISOString().substr(0,10);
}



//56
/** Scrieți o funcție cu valoare reală Leng (x_A, y_A, x_B, y_B) care returnează lungimea unui segment AB cu coordonatele date ale 
punctelor sale finale:| AB | = ((x_A - x_B) ^ 2 + (y_A - y_B) ^ 2) ^ 1/2
(x_A, y_A, x_B, y_B sunt parametri cu valoare reală). Folosind această funcție, găsiți lungimile segmentelor AB, AC, AD cu condiția 
ca coordonatele punctelor A, B, C, D să fie date. */

function leng(xA, yA, xB, yB){
    const ab = ((xA - xB) ** 2 + (yA - yB) ** 2) ** 0.5
    return parseFloat(ab.toFixed(2))
}


//57
/** Folosind funcția Leng din sarcina Func56, scrieți o funcție Perim cu valoare reală (x_A, y_A, x_B, y_B, x_C, y_C) care returnează 
 perimetrul unui triunghi ABC cu coordonatele date ale vârfurilor sale (x_A, y_A, x_B, y_B, x_C, y_C sunt parametri cu valoare reală). 
 Folosind funcția Perim, găsiți perimetrele triunghiurilor ABC, ABD, ACD cu condiția ca coordonatele punctelor A, B, C, D să fie date. */

 function perim(xA, yA, xB, yB, xC, yC){
    const ab = leng(xA, yA, xB, yB)
    const ac = leng(xA, yA, xC, yC)
    const cb = leng(xC, yC, xB, yB)
    return ab + ac + cb
}



//58
/** Folosind funcțiile Leng și Perim din sarcinile Func56 și Func57, scrieți o funcție cu valoare reală Area (x_A, y_A, x_B, y_B, x_C, y_C) 
care returnează aria unui triunghi ABC: S_ABC = (p · (p− | AB |) · (p− | AC |) · (p− | BC |)) ^ 1/2, unde p este jumătatea perimetrului. 
Folosind funcția Area, găsiți ariile triunghiurilor ABC, ABD, ACD cu condiția ca coordonatele punctelor A, B, C, D să fie date. */

function area(xA, yA, xB, yB, xC, yC){
    const p = perim(xA, yA, xB, yB, xC, yC) / 2
    const ab = leng(xA, yA, xB, yB)
    const ac = leng(xA, yA, xC, yC)
    const bc = leng(xB, yB, xC, yC)
    const sABC = (p * (p - Math.abs(ab) ) * (p - Math.abs(ac)) * (p - Math.abs(bc))) ** 0.5
    return parseFloat(sABC.toFixed(2))
}



//59
/** Folosind funcțiile Leng și Area din sarcinile Func56 și Func58, scrieți o funcție cu valoare reală Dist (x_P, y_P, x_A, y_A, x_B, y_B) 
care returnează distanța D (P, AB) între un punct P și o linie AB: D (P, AB) = 2 · S_PAB / | AB |, unde S_PAB este aria triunghiului PAB. 
Folosind această funcție, găsiți distanța dintre un punct P și fiecare dintre liniile AB, AC, BC cu condiția ca coordonatele punctelor P, A, B, C să fie date. */

function dist(xP, yP, xA, yA, xB, yB){
    const d =  2*area(xP, yP, xA, yA, xB, yB) / Math.abs(leng(xA, yA, xB, yB))
    return parseFloat(d.toFixed(2))
}


//60
/** Folosind funcția Dist din sarcina Func59, scrieți o funcție Alts (x_A, y_A, x_B, y_B, x_C, y_C) care evaluează și returnează 
altitudinile h_A, h_B, h_C trase din vârfurile A, B, C ale unui triunghi ABC (coordonatele vârfurilor sunt parametri de intrare reali). 
Folosind această funcție, evaluați altitudinile fiecărui triunghi ABC, ABD, ACD cu condiția ca coordonatele punctelor A, B, C, D să fie date. */

function alts(xA, yA, xB, yB, xC, yC){
    const hA = dist(xA, yA, xB, yB, xC, yC)
    const hB = dist(xB, yB, xA, yA, xC, yC)
    const hC = dist(xC, yC, xB, yB, xA, yA)
    return{
        h_A: hA,
        h_B: hB,
        h_C: hC
    }
}

module.exports = {
    swap,
    minmax,
    sortInc3,
    sortDec3,
    shiftRight3,
    shiftLeft3,
    power1,
    power2,
    power3,
    exp1,
    sin1,
    cos1,
    ln1,
    atan1,
    power4,
    gcd2,
    frac1,
    lcm2,
    gcd3,
    timeToHMS,
    incTime,
    isLeapYear,
    monthDays,
    prevDate,
    nextDate,
    leng,
    perim,
    area,
    dist,
    alts
}