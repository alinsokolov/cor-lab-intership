/**Function 1
 
Write an integer function Sign(X) that returns the following value:    −1, if X < 0;      0, if X = 0;      1,  if X > 0
(X is a real-valued parameter). Using this function, evaluate an expression Sign(A) + Sign(B) for given real numbers A and B. **/

function Sign(x){
    if (x<0) {
        return -1;
    }
    if (x>0) {
        return 1
    }
    return 0;
}

function f1(){
    let a = document.getElementById("f1_A").value;
    let b = document.getElementById("f1_B").value;
    document.getElementById("rezult").innerHTML = Sign(a) + Sign(b);

}

/** Function 2
 
Write an integer function RootCount(A, B, C) that returns the amount of roots of the quadratic equation 
A·x^2 + B·x + C = 0 (A, B, C are real-valued parameters, A ≠ 0). 
Using this function, find the amount of roots for each of three quadratic equations with given coefficients. 
Note that the amount of roots is determined by the value of a discriminant:
D = B^2 − 4·A·C. **/

function RootCount(a,b,c){
    if (a==0) {
        return "Introdu un numar A diferit de 0";
    }
    const D = b*b-4*a*c;
    if (D>0) {
        return 2;
    } 
    if (D==0) {
        return 1;
    }
    return 0;
}

function f2(){
    let a = document.getElementById("f2_A").value;
    let b = document.getElementById("f2_B").value;
    let c = document.getElementById("f2_C").value;
    document.getElementById("rezult2").innerHTML = RootCount(a,b,c);

}

/** Function 3
 
Write a real-valued function CircleS(R) that returns the area of a circle of radius R (R is a real number).
Using this function, find the areas of three circles of given radiuses. 
Note that the area of a circle of radius R can be found by formula S = π·R^2. Use 3.14 for a value of π. **/


function CircleS(r){
    return Math.PI*(r*r);
}

function f3(){
    let r = document.getElementById("f3_R").value;
    document.getElementById("rezult3").innerHTML = `Aria = ${CircleS(r)}`;
}

/** Function 4
 
Write a real-valued function RingS(R1, R2) that returns the area of a ring bounded by a concentric circles 
of radiuses R1 and R2 (R1 and R2 are real numbers, R1 > R2).
Using this function, find the areas of three rings with given outer and inner radiuses. 
Note that the area of a circle of radius R can be found by formula S = π·R^2. Use 3.14 for a value of π. **/


function RingS(r1,r2){
    const areaBig = Math.PI*Math.max(r1,r2)**2;
    const areaSmall = Math.PI*Math.min(r1,r2)**2;
    const areaRing = areaBig - areaSmall;
    return areaRing;
}

function f4(){
    let r1 = document.getElementById("f4_R1").value;
    let r2 = document.getElementById("f4_R2").value;
    document.getElementById("rezult4").innerHTML = `The area of a ring bounded by a concentric circles is ${RingS(r1,r2)}`;
}

/** Function 5
 
 Write a real-valued function TriangleP(a, h) that returns the perimeter of an isosceles triangle with given base a
 and altitude h (a and h are real numbers). Using this function, find the perimeters of three triangles with given bases and altitudes. 
 Note that the leg b of an isosceles triangle can be found by the Pythagorean theorem:
 b^2 = (a/2)^2 + h^2. **/


 function TriangleP(a, h){
    const hypotenuse = Math.sqrt((a/2)**2 + h**2);
    const perimeter = hypotenuse*2 + a;
    return perimeter;
}

function f5(){
    let a = document.getElementById("f5_a").value;
    let h = document.getElementById("f5_h").value;
    document.getElementById("rezult5").innerHTML = `The perimeter of the isosceles triangle is ${TriangleP(a, h)}`;
}


/** Function 6
 
 Write an integer function SumRange(A, B) that returns a sum of all integers in the range A to B inclusively (A and B are integers). 
 In the case of A > B the function returns 0. Using this function, find a sum of all integers in the range A to B and 
 in the range B to C provided that integers A, B, C are given. **/


 function SumRange(a, b){
    let interval = 0;
    a = parseInt(a);
    b = parseInt(b);
    for(let i=a; i<=b; i++){
       interval += parseInt(i);
    }
    return interval;
}


function f6(){
    let a = document.getElementById("f6_a").value;
    let b = document.getElementById("f6_b").value;
    let c = document.getElementById("f6_c").value;

    document.getElementById("rezult6").innerHTML = SumRange(a, b);
    document.getElementById("rezult6.2").innerHTML = SumRange(b, c);

    if (!a) {
        document.getElementById("rezult6").innerHTML = "";
    }
    if (!b){
        document.getElementById("rezult6").innerHTML = "";
        document.getElementById("rezult6").innerHTML = "";
    }
    if (!c){
        document.getElementById("rezult6.2").innerHTML = "";
    }
}

/** Function 7
 
 Write a real-valued function Calc(A, B, Op) that performs an arithmetic operation over nonzero real numbers A and B 
 and returns the result value. An arithmetic operation is determined by integer parameter Op as follows: 1 — subtraction, 
 2 — multiplication, 3 — division, and addition otherwise. Having input real numbers A, B and integers N1, N2, N3 and using 
 this function, perform over given A, B three operations determined by given N1, N2, N3. Output the result value of each operation. **/

 function Calc(a, b, op){
     switch(op){
         case "1":
            return a-b;
         case "2":
             return a*b;
         case "3":
             return a/b;
         default:
            return parseInt(a) + parseInt(b) ;
     }
 }

 function f7(){
    let a = document.getElementById("f7_a").value;
    let b = document.getElementById("f7_b").value;
    let n1 = document.getElementById("f7_n1").value;
    let n2 = document.getElementById("f7_n2").value;
    let n3 = document.getElementById("f7_n3").value;
    
    document.getElementById("rezult7.1").innerHTML = Calc(a, b, n1);
    document.getElementById("rezult7.2").innerHTML = Calc(a, b, n2);
    document.getElementById("rezult7.3").innerHTML = Calc(a, b, n3);
}

/** Function 8

Write an integer function Quarter(x, y) that returns the number of a coordinate quarter containing a point with nonzero 
real-valued coordinates (x, y). Using this function, find the numbers of coordinate quarters containing each of three
 points with given nonzero coordinates. **/

 function Quarter(x, y){
     if (x>0 && y>0){
        return 1;
     } 
     if (x<0 && y>0){
        return 2;
     } 
     if (x<0 && y<0){
        return 3;
     } 
        return 4;
 }

 function f8(){

     let x = document.getElementById("f8_x").value;
     let y = document.getElementById("f8_y").value;
     let x2 = document.getElementById("f8_x2").value;
     let y2 = document.getElementById("f8_y2").value;
     let x3 = document.getElementById("f8_x3").value;
     let y3 = document.getElementById("f8_y3").value;
    
     document.getElementById("rezult8").innerHTML = "Primul numar se afla in cadranul " + Quarter(x,y);
     document.getElementById("rezult8.2").innerHTML = "Al doilea numar se afla in cadranul " + Quarter(x2,y2);
     document.getElementById("rezult8.3").innerHTML = "Al al treilea numar se afla in cadranul " + Quarter(x3,y3);

 }
/** Function 9

Write a logical function Even(K) that returns True, if an integer parameter K is an even number, and False otherwise. 
Using this function, find the amount of even numbers in a given sequence of 10 integers. **/

 function Even(k){
    return k%2 == 0;
 }

 function f9(){
    let allStrNum = document.getElementById("f9_k").value;
    let arrayNum = allStrNum.split(" ");
    let even = 0;

    for(i=0; i<arrayNum.length; i++){
      if (Even(arrayNum[i]) == true) even++;
    }

    document.getElementById("rezult9").innerHTML = `Din acest sir ${even} numere sunt pare`;

    if(!allStrNum) {
        document.getElementById("rezult9").innerHTML = "Introdu numerele";
    }
}

 /** Function 10

Write a logical function IsSquare(K) that returns True, if an positive integer parameter K is a square of some integer, and False otherwise. 
Using this function, find the amount of squares in a given sequence of 10 positive integers. **/

 function IsSquare(k){
    return Number.isInteger(Math.sqrt(k));
 }

 function f10(){
    let allStrNum = document.getElementById("f10_k").value;
    let arrayNum = allStrNum.split(" ");
    let square = 0;
   
    for(i=0; i<arrayNum.length; i++){
        if (IsSquare(arrayNum[i]) == true) square++;
      }

      document.getElementById("rezult10").innerHTML = `Din acest sir ${square} sunt patrare ale altor numere intregi`;

      if(!allStrNum) {
          document.getElementById("rezult10").innerHTML = "Introdu numerele";
      }
  }
  
 /** Function 11
  
Write a logical function IsPower5(K) that returns True, if an positive integer parameter K is equal to 5 raised to some integer power, 
and False otherwise. Using this function, find the amount of powers of base 5 in a given sequence of 10 positive integers. **/

function  IsPower5(k){
    const log5K = Math.log(k) / Math.log(5);
    return Number.isInteger(log5K); 
}

function f11(){
    let allStrNum = document.getElementById("f11_k").value;
    let arrayNum = allStrNum.split(" ");
    let isPower5 = 0;
   
    if(!allStrNum) {
        return document.getElementById("rezult11").innerHTML = "Introdu numerele";
    }
    for(i=0; i<arrayNum.length; i++){
        if (IsPower5(arrayNum[i]) == true) isPower5++;
      }

      document.getElementById("rezult11").innerHTML = `Din acest sir ${isPower5} numere sunt puteri in baza 5`;
  }

  
  /** Function 12
  
Write a logical function IsPowerN(K, N) that returns True, if an positive integer parameter K is equal to N (> 1) raised to
some integer power, and False otherwise. Having input an integer N (> 1) and a sequence of 10 positive integers and using 
this function, find the amount of powers of base N in the given sequence. **/

function  IsPowerN(k,n){
    const logNk = Math.log(k) / Math.log(n);
    return Number.isInteger(logNk);
}

function f12(){
    let allStrNum = document.getElementById("f12_k").value;
    let arrayNum = allStrNum.split(" ");
    let isPowerN = 0;
    let base_n = document.getElementById("f12_n").value
   
    if(!allStrNum || !base_n) {
       return document.getElementById("rezult12").innerHTML = "Introdu numerele";
    }
    if(base_n <= 1){
         return document.getElementById("rezult12").innerHTML = "Introdu baza N>1";
    }
    for(i=0; i<arrayNum.length; i++){
        if (IsPowerN(arrayNum[i],base_n) == true) isPowerN++;
      }
      document.getElementById("rezult12").innerHTML = `Din acest sir ${isPowerN} numere sunt puteri in baza ${base_n}`;
  }

  
  
 /** Function 13
  
Write a logical function IsPrime(N) that returns True, if an integer parameter N (> 1) is a prime number, and False otherwise. 
Using this function, find the amount of prime numbers in a given sequence of 10 integers greater than 1. 
Note that an integer (> 1) is called a prime number if it has not positive divisors except 1 and itself.**/

function isPrime(n) {
    if (n<2){
        return false;
    }
    if (n==2){
        return true;
    }
    if (n>2){
        for (let i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }  
        }
    }
    return true;
}

function f13(){
    let allStrNum = document.getElementById("f13_n").value;
    let arrayNum = allStrNum.split(" ");
    let is_prime = 0;
   
    if(!allStrNum) {
       return document.getElementById("rezult13").innerHTML = "Introdu numerele";
    }
    for(i=0; i<arrayNum.length; i++){
        if (isPrime(arrayNum[i]) == true) is_prime++;
      }
      document.getElementById("rezult13").innerHTML = `Din acest sir ${is_prime} sunt numere prime `;
  }


 /** Function 14
  
Write an integer function DigitCount(K) that returns the amount of digits in the decimal representation of a positive integer K.
Using this function, find the amount of digits for each of five given positive integers.**/

function DigitCount(k)
{
     return parseInt(Math.floor( Math.log(k) / Math.log(10)) + 1);   
}

 function f14(){
    let allStrNum = document.getElementById("f14_k").value;
    let arrayNum = allStrNum.split(" ");
    
    for(let i = 0; i<arrayNum.length;i++){
       document.write(`Numarul de cifre a numarului ${arrayNum[i]} in baza 10 este ${DigitCount(arrayNum[i])} <br>`);
    }
 }


 /** Function 15
  
Write an integer function DigitN(K, N) that returns the N-th digit in the decimal representation of a positive integer K provided 
that the digits are numbered from right to left. If the amount of digits is less than N then the function returns −1. Using this 
function, output sequentially 1st, 2nd, 3rd, 4th, 5th digit for each of five given positive integers K1, K2, …, K5.**/

function DigitN(k,n){
    const AMOUNT_DIGIT = parseInt(Math.floor( Math.log(k) / Math.log(10)) + 1);
    if (AMOUNT_DIGIT < n){
        return -1;
    }
    let revArrayNum = k.toString().split("").reverse();
    let n_th = revArrayNum[n-1];
    return n_th;
}

function f15(){
    let allStrNum = document.getElementById("f15_k").value;
    let arrayNum = allStrNum.split(" ");
    
    
    for(let i=0; i<arrayNum.length;i++){
        //verifica ca numerele sa nu fie negative sau nule
        for(let g=0; g<arrayNum.length;g++){
            if(arrayNum[g]<1){
                return document.getElementById("rezult15").innerHTML = `Introdu numere pozitive nenule`;
            }
        }
        //apelarea functiei
        for(let j=1; j<=5; j++){
            let n_th = DigitN(arrayNum[i],j);
            if(j==1){
                document.write("<br>");
            }
            if(n_th == -1){
                document.write("");
            }
            else{
                document.write(`${j}st cifra de la dreapta la stanga al nr ${arrayNum[i]} este ${n_th} <br>`);
            }
        }
     }
}


/** Function 16
  
Write a logical function IsPalindrome(K) that returns True, if the decimal representation of a positive 
parameter K is a palindrome (i. e., it is read equally both from left to right and from right to left), 
and False otherwise. Using this function, find the amount of palindromes in a given sequence of 10 positive integers.**/

function IsPalindrome(k){
    let copie = k;
    let ogl = 0;

    while(k!=0){
      ogl = ogl*10 + (k%10);
      k = Math.floor(k/10);
    }

    return ogl==copie;
}


function f16(){
    let allStrNum = document.getElementById("f16_k").value;
    let arrayNum = allStrNum.split(" ");
    let is_polindrom = 0;
   
    if(!allStrNum) {
       return document.getElementById("rezult16").innerHTML = "Introdu numerele";
    }
    for(i=0; i<arrayNum.length; i++){
        if (IsPalindrome(arrayNum[i]) == true) is_polindrom++;
      }
      document.getElementById("rezult16").innerHTML = `Din acest sir ${is_polindrom} sunt polindroame `;
 }


/** Function 17
  
Write a real-valued function DegToRad(D) that converts the angle value D in degrees into the one in radians 
(D is a real number, 0 ≤ D < 360). Note that 180° = π radians and use 3.14 for a value of π. Using this function, 
convert five given angles from degrees into radians. **/

function DegToRad(d){
    return d*(Math.PI/180);
}

function f17(){
    let allStrNum = document.getElementById("f17_d").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";
   
    if(!allStrNum) {
       return document.getElementById("rezult17").innerHTML = "Introdu numerele";
    }
    return arrayNum.reduce(function (prev,item,index){
      if(item>=0 && item<360){
        document.getElementById("rezult17").innerHTML = rezult+= `${item} grade = ${DegToRad(item)} radiani <br>`;
      }
      else {
        document.getElementById("rezult17").innerHTML = rezult+= `${item} nu este o valoare valida <br>`;
      }  
    }, 0)
}


/** Function 18

Write a real-valued function RadToDeg(R) that converts the angle value R in radians into the one in degrees 
(R is a real number, 0 ≤ R < 2·π). Note that 180° = π radians and use 3.14 for a value of π. Using this function, 
convert five given angles from radians into degrees.**/

function RadToDeg(d){
    return d*(180/Math.PI);
}

function f18(){
    let allStrNum = document.getElementById("f18_d").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";
   
    if(!allStrNum) {
       return document.getElementById("rezult18").innerHTML = "Introdu numerele";
    }
    return arrayNum.reduce(function (prev,item,index){
      if(item>=0 && item<(2*Math.PI)){
        document.getElementById("rezult18").innerHTML = rezult+= `${item} radiani = ${RadToDeg(item)} grade <br>`;
      }
      else {
        document.getElementById("rezult18").innerHTML = rezult+= `${item} nu este o valoare valida <br>`;
      }  
    }, 0)
}


/** Function 19

Func19. Write a real-valued function Fact(N) that returns a factorial of a positive integer N: N! = 1·2·…·N 
(the real return type allows to avoid the integer overflow during the calculation of the factorials for large values of the parameter N). 
Using this function, find the factorials of five given integers.**/

function Fact(n){
    if (n == 0 || n == 1) {
        return 1;
    }

    for (let i = n - 1; i >= 1; i--) {
        n *= i;
    }
    return n;
}

function f19(){
    let allStrNum = document.getElementById("f19_n").value;
    let array = allStrNum.split(" ");
    let rezult = "";

    if(!allStrNum) {
       return document.getElementById("rezult19").innerHTML = "Introdu numerele";
    }
    return array.reduce(function (prev,item,index){
        if(item >= 0 && (item % 2 == 0 || item % 2 == 1)) {
              return document.getElementById("rezult19").innerHTML = rezult+= `${item}! = ${Fact(item)} <br>`;
        }
        else {
            return document.getElementById("rezult19").innerHTML = rezult+= `${item} nu este o valoare valida <br>`;
        }  
    }, 0)
}


/** Function 20

Write a real-valued function Fact2(N) that returns a double factorial N!!:

N!! = 1·3·5·…·N,    if N is an odd number;
N!! = 2·4·6·…·N    otherwise

(N is a positive integer; the real return type allows to avoid the integer overflow during the calculation of the double 
factorials for large values of N). Using this function, find the double factorials of five given integers.**/

function Fact2(n){
    if (n == 0 || n == 1) {
        return 1;
    }

    for (let i = n-2; i >= 1; i = i-2) {
        n *= i;
    }
    return n;
}

function f20(){
    let allStrNum = document.getElementById("f20_n").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";
   
    if(!allStrNum) {
       return document.getElementById("rezult20").innerHTML = "Introdu numerele";
    }
    return arrayNum.reduce(function (prev,item,index){
      if(item >= 0 && (item % 2 == 0 || item % 2 == 1)) {
        document.getElementById("rezult20").innerHTML = rezult+= `${item}!! = ${Fact2(item)} <br>`;
      }
      else {
        document.getElementById("rezult20").innerHTML = rezult+= `${item} nu este o valoare valida <br>`;
      }  
    }, 0)
}


/** Function 21

Write an integer function Fib(N) that returns the value of N-th term of the sequence of the Fibonacci numbers. The Fibonacci numbers FK 
are defined as follows:

F1 = 1,        F2 = 1,        FK = FK−2 + FK−1,    K = 3, 4, … .

Using this function, find five Fibonacci numbers with given order numbers N1, N2, …, N5.**/

function Fib(n){
    let f1 = 0, f2 = 1, nextTerm = 0;
    for (let i = 2; i <= n; i++) {
        nextTerm = f1 + f2;
        f1 = f2;
        f2 = nextTerm;
    }
    return f1;
}

function f21(){
    let allStrNum = document.getElementById("f21_n").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";
   
    if(!allStrNum) {
       return document.getElementById("rezult21").innerHTML = "Introdu numerele";
    }
    return arrayNum.reduce(function (prev,item,index){
      if(item > 0 && (item % 2 == 0 || item % 2 == 1)) {
        document.getElementById("rezult21").innerHTML = rezult+= `Al ${item} NR Fibonacci = ${Fib(item)} <br>`;
      }
      else {
        document.getElementById("rezult21").innerHTML = rezult+= `${item} nu este o valoare valida <br>`;
      }  
    }, 0)
}


/** Function 22

 Write a function PowerA3(A) that returns the third degree of a real number A (A is an input parameter). 
 Using this function, find the third degree of five given real numbers.**/

 function powerA3(a){
    return a**3;
 }

 function f22(){
    let allStrNum = document.getElementById("f22_n").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";
   
    if(!allStrNum) {
       return document.getElementById("rezult22").innerHTML = "Introdu numerele";
    }
    return arrayNum.reduce(function (prev,item,index){
      if(Number.isInteger( parseInt(item))) {
        document.getElementById("rezult22").innerHTML = rezult+= `${item}^3 = ${powerA3(item)} <br>`;
      }
      else {
        document.getElementById("rezult22").innerHTML = rezult+= `${item} nu este o valoare valida <br>`;
      }  
    }, 0)
}

/** Function 23

Write a function PowerA234(A) that computes the second, the third, and the fourth degrees of 
a real number A and returns these degrees as three real numbers (A is an input parameter). 
Using this function, find the second, the third, and the fourth degrees of five given real numbers.**/

function powerA234(a){
    let p234 = [2,3,4];
    for(i=0; i<p234.length; i++){
        p234[i] = a**p234[i];
    }
    return p234;
}

function f23(){
    let allStrNum = document.getElementById("f23_n").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";
   
    if(!allStrNum) {
       return document.getElementById("rezult23").innerHTML = "Introdu numerele";
    }
    return arrayNum.reduce(function (prev,item,index){
      if(Number.isInteger( parseInt(item))) {
        document.getElementById("rezult23").innerHTML = rezult+= `${item}^2, ${item}^3, ${item}^4, = ${powerA234(item)} <br>`;
      }
      else {
        document.getElementById("rezult23").innerHTML = rezult+= `${item} nu este o valoare valida <br>`;
      }  
    }, 0)
}


/** Function 24
Write a function Mean(X, Y) that computes the arithmetical mean (X+Y)/2 and the geometrical mean (X·Y)^1/2 of two positive real 
numbers X and Y and returns the result as two real numbers (X and Y are input parameters). Using this function, find the arithmetical 
mean and the geometrical mean of pairs (A, B), (A, C), (A, D) provided that real numbers A, B, C, D are given.**/

function mean(x, y){
    const mAr = (x + y) / 2;
    const mGeo = (x * y) ** 0.5;
    return [mAr, mGeo]; 
}

function f24(){
    let allStrNum = document.getElementById("f24_n").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";

    for(i=0; i<arrayNum.length; i++){
        arrayNum[i] = parseInt(arrayNum[i]);
    }
    if(!allStrNum) {
        return document.getElementById("rezult24").innerHTML = "Introdu numerele";
     }
    document.getElementById("rezult24").innerHTML = rezult+= `(A,B) Media aritmetica = ${mean(arrayNum[0],arrayNum[1])[0]} <br> (A,B) Media geometrica = ${mean(arrayNum[0], arrayNum[1])[1]} <br>`;
    document.getElementById("rezult24").innerHTML = rezult+= `(A,C) Media aritmetica = ${mean(arrayNum[0],arrayNum[2])[0]} <br> (A,C) Media geometrica = ${mean(arrayNum[0], arrayNum[2])[1]} <br>`;
    document.getElementById("rezult24").innerHTML = rezult+= `(A,D) Media aritmetica = ${mean(arrayNum[0],arrayNum[3])[0]} <br>(A,D) Media geometrica = ${mean(arrayNum[0], arrayNum[3])[1]} <br>`;
}



/** Function 25
Write a function TrianglePS(a) that computes the perimeter P = 3·a and the area S = a^2·(3^1/2)/4 of an equilateral triangle with 
the side a and returns the result as two real numbers (a is an input real-valued parameter). Using this function, find the 
perimeters and the areas of three triangles with the given lengths of the sides.**/

function trianglePS(a){
    const p = 3*a;
    const s = a**2 * (3**0.5 / 4);
    return [p,s];
}

function f25(){
    let allStrNum = document.getElementById("f25_n").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";
 
    if(!allStrNum) {
        return document.getElementById("rezult25").innerHTML = "Introdu numerele";
     }
     for(i=0; i<arrayNum.length; i++){
        document.getElementById("rezult25").innerHTML = rezult+= `Latura = ${arrayNum[i]} , Perimetrul = ${trianglePS(arrayNum[i])[0]} , Aria = ${trianglePS(arrayNum[i])[1]} <br>`;
     }
}


/** Function 26
Write a function RectPS(x1, y1, x2, y2) that computes and returns the perimeter P and the area S of a rectangle whose opposite vertices 
have coordinates (x1, y1) and (x2, y2) and sides are parallel to coordinate axes (x1, y1, x2, y2 are input real-valued parameters). 
Using this function, find the perimeters and the areas of three rectangles with the given opposite vertices.**/

function rectPS(x1,y1,x2,y2){
    const a = Math.abs(x1-x2);
    const b = Math.abs(y1-y2);
    const area = a*b;
    const perimeter = 2*a + 2*b;
    return [area,perimeter];
}

function f26 () {
    let x1 = document.getElementById("f26_x1").value;
    let y1 = document.getElementById("f26_y1").value;
    let x2 = document.getElementById("f26_x2").value;
    let y2 = document.getElementById("f26_y2").value;
    document.getElementById('rezult26').innerHTML = `Perimetrul dreptunghiului = ${rectPS(x1,y1,x2,y2)[0]} <br> Aria dreptunghiului = ${rectPS(x1,y1,x2,y2)[1]}`;
  }
  

/** Function 27
Write a function DigitCS(K) that finds and returns the amount C of digits in the decimal representation of a positive integer K and 
also the sum S of its digits (K is an input integer parameter). Using this function, find the amount and the sum of digits for each 
of five given integers.**/

function digitCS(k){
    let count = 0;
    let sum = 0;
    while (k != 0){
        sum += (k % 10);
        k = Math.floor(k / 10);
        count++;
    }
    return [count,sum];
}

function f27(){
    let allStrNum = document.getElementById("f27_n").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";
 
    if(!allStrNum) {
        return document.getElementById("rezult27").innerHTML = "Introdu numerele";
     }
     return arrayNum.reduce(function (prev,item,index){
        if(item > 0 && (item % 2 == 0 || item % 2 == 1)){
            document.getElementById("rezult27").innerHTML = rezult += 
            `Numarul de cifre pentru ${item} = ${digitCS(item)[0]}  <br>
            Suma cifrelor pentru ${item} = ${digitCS(item)[1]} <br>` 
        }
     }, 0)
}

/** Function 28
Write a function InvDigits(K) that inverts the order of digits of a positive integer K and returns the obtained integer 
(K is an input parameter). Using this function, invert the order of digits for each of five given integers.**/

function invDigits(k){
    let ogl = 0;
    while(k!=0){
      ogl = ogl*10 + (k%10);
      k = Math.floor(k/10);
    }
    return ogl;
}

function f28(){
    let allStrNum = document.getElementById("f28_k").value;
    let arrayNum = allStrNum.split(" ");
    let rezult = "";
   
    if(!allStrNum) {
       return document.getElementById("rezult28").innerHTML = "Introdu numerele";
    }
    return arrayNum.reduce(function (prev,item,index){
        if(item > 0 && (item % 2 == 0 || item % 2 == 1)){
            document.getElementById("rezult28").innerHTML = rezult += `Inversul lui ${item} = ${invDigits(item)}  <br>`;
        }
     }, 0)
 }

 /** Function 29
Write a function AddRightDigit(D, K) that adds a digit D to the right side of the decimal representation of a positive integer K 
and returns the obtained number (D and K are input integer parameters, the value of D is in the range 0 to 9). Having input an 
integer K and two one-digit numbers D1, D2 and using two calls of this function, sequentially add the given digits D1, D2 to the 
right side of the given K and output the result of each adding.**/

function addRightDigit(d, k){
    return parseInt(k) * 10 + parseInt(d);
}

function f29(){
	let k = document.getElementById("f29_k").value;
	let d1 = document.getElementById("f29_d1").value;
	let d2 = document.getElementById("f29_d2").value;

    if(!k || !d1 || !d2) {
        return document.getElementById("rezult29").innerHTML = "Introdu numerele";
     }
    if(d1,d2 > 0 && d1,d2 < 9){
	    document.getElementById('rezult29').innerHTML= `Adaugare ${d1}: ${addRightDigit(d1, k)} <br> Adaugare ${d2}: ${addRightDigit(d2, k)}`; 
    }
}


 /** Function 30
Write a function AddLeftDigit(D, K) that adds a digit D to the left side of the decimal representation of a positive 
integer K and returns the obtained number (D and K are input integer parameters, the value of D is in the range 0 to 9). 
Having input an integer K and two one-digit numbers D1, D2 and using two calls of this function, sequentially add the given 
digits D1, D2 to the left side of the given K and output the result of each adding.**/

function addLeftDigit(d, k){
    k = k.toString();
    d = d.toString();
    return parseInt(d+k);
}

function f30(){
	let k = document.getElementById("f30_k").value;
	let d1 = document.getElementById("f30_d1").value;
	let d2 = document.getElementById("f30_d2").value;

    if(!k || !d1 || !d2) {
        return document.getElementById("rezult30").innerHTML = "Introdu numerele";
     }
    if(d1,d2 > 0 && d1,d2 < 9){
	    document.getElementById("rezult30").innerHTML= `Adaugare ${d1}: ${addLeftDigit(d1, k)} <br> Adaugare ${d2}: ${addLeftDigit(d2, k)}`; 
    }
}